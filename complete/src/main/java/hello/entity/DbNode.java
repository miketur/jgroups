package hello.entity;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.List;

@Entity
@Table(name = "NODE")
public class DbNode {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(name = "NODE_ID")
    private BigInteger nodeId;

    @Column(name = "LATITUDE")
    private Float latitude;

    @Column(name = "LONGITUDE")
    private Float longitude;

    @Override
    public String toString() {
        return String.format(
                "Node info [id=%d, latitude='%f', longitude='%f']",
                id, latitude, longitude);
    }

    public DbNode() {
    }

    public DbNode(BigInteger nodeId, Float latitude, Float longitude) {
        this.nodeId = nodeId;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Long getId() {
        return id;
    }

    public BigInteger getNodeId() {
        return nodeId;
    }

    public Float getLatitude() {
        return latitude;
    }

    public Float getLongitude() {
        return longitude;
    }
}
