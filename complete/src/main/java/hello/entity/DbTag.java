package hello.entity;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table(name = "TAG")
public class DbTag {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(name = "KEY")
    private String key;

    @Column(name = "VALUE")
    private String value;

    @Column(name = "NODE_ID")
    private BigInteger nodeId;

    @Override
    public String toString() {
        return String.format(
                "Tags info [node_id=%d, key='%s', value='%s']",
                nodeId, key, value);
    }

    public DbTag() {
    }


    public DbTag(String key, String value, BigInteger nodeId) {
        this.key = key;
        this.value = value;
        this.nodeId = nodeId;
    }

    public Long getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public BigInteger getNodeId() {
        return nodeId;
    }
}
