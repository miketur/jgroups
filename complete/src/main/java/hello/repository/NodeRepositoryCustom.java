package hello.repository;

import hello.entity.DbNode;

import java.util.List;

public interface NodeRepositoryCustom {
    public List<DbNode> findDbNodeInRadius(double lat, double lot, double radius);
}
