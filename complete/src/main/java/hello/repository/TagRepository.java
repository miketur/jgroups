package hello.repository;

import hello.entity.DbTag;
import org.springframework.data.repository.CrudRepository;

import java.math.BigInteger;
import java.util.List;

public interface TagRepository extends CrudRepository<DbTag, Long> {

    List<DbTag> findByNodeId(BigInteger nodeId);
}
