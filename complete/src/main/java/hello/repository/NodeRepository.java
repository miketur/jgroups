package hello.repository;

import hello.entity.DbNode;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NodeRepository extends CrudRepository<DbNode, Long>, NodeRepositoryCustom {
}
