package hello;

import hello.entity.DbNode;
import hello.entity.DbTag;
import hello.repository.NodeRepository;
import hello.repository.TagRepository;
import hello.schema.Node;
import hello.schema.Osm;
import hello.schema.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.FileInputStream;
import java.util.*;

@SpringBootApplication
public class Application {

	private static final Logger log = LoggerFactory.getLogger(Application.class);
	private static final boolean needInit = false;

	public static void main(String[] args) {
		SpringApplication.run(Application.class);
	}

	@Bean
	public CommandLineRunner demo(NodeRepository nodeRepository, TagRepository tagRepository) {
		return (args) -> {

			if(needInit) {
				parse(nodeRepository, tagRepository);
			} else {
				log.info("START TO FIND NODE IN CURRENT RADIUS");
				List<DbNode> dbNodes = nodeRepository.findDbNodeInRadius(55.0282211303711, 82.9234466552734, 100);
				for(DbNode dbNode: dbNodes) {
					log.info(dbNode.toString());
					for(DbTag dbTags: tagRepository.findByNodeId(dbNode.getNodeId())) {
						log.info(dbTags.toString());
					}
					log.info("");
				}
			}
		};
	}

	private void parse(NodeRepository nodeRepository, TagRepository tagRepository) {
		try {
			// create JAXBContext for the primer.xsd
			JAXBContext context = JAXBContext.newInstance(Osm.class);

			Unmarshaller unmarshaller = context.createUnmarshaller();

			unmarshaller.setListener(new Unmarshaller.Listener() {
				public void afterUnmarshal(Object target, Object parent) {
					if (target instanceof Node) {
						Node node = (Node) target;

						for(Tag tag: node.getTag()) {
							tagRepository.save(new DbTag(tag.getK(), tag.getV(), node.getId()));
						}
						nodeRepository.save(new DbNode(node.getId(), node.getLat(), node.getLon()));
					}

				}
			});

			// create a new XML parser
			SAXParserFactory factory = SAXParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XMLReader reader = factory.newSAXParser().getXMLReader();
			reader.setContentHandler(unmarshaller.getUnmarshallerHandler());
			reader.parse(new InputSource(new FileInputStream(new File("RU-NVS.osm"))));


		} catch (Exception e) {
			throw new RuntimeException(e);
		}


	}

}
